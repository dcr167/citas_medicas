<!DOCTYPE html>
<html>
    <head>
    <title>Citas|Medicas</title>
        
        <link rel="stylesheet" type="text/css" media="screen" href="css/stmodificar_pacientes.css" />
        <link rel="stylesheet" type="text/css" href="css/estilos.css">
        <link rel="stylesheet" href="css/st_cerrar_sesion.css">
	    <link rel="stylesheet" href="css/fontello.css">
    
    </head>
    <body>
    <section class="title">
        <a href="inicio.php"><h1 class="icon-medkit">Citas Medicas</h1></a>
        <div class="ad">
		<?php

session_start();
if(isset($_SESSION['u_user'])){
	echo "<h2><i class='icon-user-1'></i>Administradror</h2>";

	echo"<a href='cerrar_sesion_login.php'><i class='icon-off'></i>Cerrar sesion</a>";
}else{
	header("Location:index.php");

}

?>
</div>
	</section>
	<nav class="nave">
		<ul class="menu">
			<li class="first-item">
				<a href="inicio.php">
                    <div class="ini">
                    <label for="" class="icon-home" ></label>
                    </div>
                      
					<span class="text-item">Inicio</span>
					<span class="down-item"></span>
				</a>
			</li>

			<li>
				<a href="c_citas.php">
                    <div class="h">
                <label for="" class="icon-heartbeat"></label>
                </div>
					
					<span class="text-item">Citas</span>
					<span class="down-item"></span>
				</a>
			</li>

			<li>
				<a href="c_pacientes.php">
                    <div class="pc">
                <label for="" class="icon-bed" ></label>
                </div>
					
					<span class="text-item">Pacientes</span>
					<span class="down-item"></span>
				</a>
			</li>

			<li>
				<a href="c_doctores.php">
                    <div class="doc">
                <label for="" class="icon-user-md" ></label>
                </div>
				
					<span class="text-item">Doctores</span>
					<span class="down-item"></span>
				</a>
			</li>

			<li>
				<a href="c_consultorios.php">
                    <div class="c">
                    <label for="" class="icon-hospital"></label>
                    </div>
					<span class="text-item">Consultorios</span>
					<span class="down-item"></span>
				</a>
			</li>
			

			
		</ul>

	</nav>


        
   
                    <?php
                    $num_paciente=$_REQUEST['num_paciente'];
                    include("connect_rcitas.php");
                    $query="SELECT * FROM pacientes WHERE num_paciente='$num_paciente'";
                    $resultado=$conexion->query($query);
                    $row=$resultado->fetch_assoc();
                        
                    ?>
            <div class="fr">
            <h1>Modificar Paciente</h1>
            <form action="modificarproceso_pacientes.php?num_paciente=<?php echo $row['num_paciente']; ?>" method="POST">

            <label for="nombres">Nombres</label>
            <input type="text" name="nombres_paci"  value="<?php echo $row['nombre_paciente'];?>"required/>
            <label for="apellidos">Apellidos</label>
            <input type="text" name="apellidos_paci"  value="<?php echo $row['apellido_paciente'];?>"required/>
            <label for="ci">CI</label>
            <input type="text" name="ci_paci"  value="<?php echo $row['ci_paciente'];?>"required/>
            <label for="edad">Edad</label>
            <input type="text" name="edad_paci"  value="<?php echo $row['edad_paciente'];?>"required/>
            <label for="sexo">Sexo</label>
            <select name="sexo_paci" id="#" value="<?php echo $row['sexo_paciente'];?>"required/>
				<option value="Masculino">Masculino</option>
				<option value="Femenino">Femenino</option>
			</select>
           
            <label for="telefono">Telefono</label>
            <input type="text" name="telefono_paci"  value="<?php echo $row['telefono_paciente'];?>"required/>
            <label for="correo">Correo</label>
            <input type="text" name="correo_paci"  value="<?php echo $row['correo_paciente'];?>"required/>
            <label for="direccion">Direccion</label>
            <input type="text" name="direccion_paci" value="<?php echo $row['direccion_paciente'];?>"required/>
            
            <input type="submit" value="Guardar">
            </form>
            </div>
            
        
        
    
    
    </body>

</html>