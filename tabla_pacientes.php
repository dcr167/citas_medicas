<!DOCTYPE html>
<html>
    <head>
	<title>Citas|Medicas</title>
        
        <link rel="stylesheet" type="text/css" media="screen" href="css/st_tabla_pacientes_doctores.css" />
		<link rel="stylesheet" type="text/css" href="css/estilos.css">
		<link rel="stylesheet" type="text/css" media="screen" href="css/st_tab_sesion.css" />
        <link rel="stylesheet" href="css/fontello.css">
        
    
    
    </head>
    <body>
	<div class="tab">
		<?php

session_start();
if(isset($_SESSION['u_user'])){
	echo "<h2><i class='icon-user-1'></i>Administradror</h2>";

	echo"<a href='cerrar_sesion_login.php'><i class='icon-off'></i>Cerrar sesion</a>";
}else{
	header("Location:index.php");

}

?>
</div>
    


        <div id="main">
            <table>
                <thead>
                    <tr>
                        <th colspan="1"><a href="c_rpacientes.php">Nuevo</a></th>
                        <th colspan="10">Lista de pacientes</th>
                    
                    </tr>
                
                </thead>
                <?php
                    include("connect_rpacientes.php");

                    $tabla="";
$query="SELECT * FROM pacientes ORDER BY num_paciente";

///////// LO QUE OCURRE AL TECLEAR SOBRE EL INPUT DE BUSQUEDA ////////////
if(isset($_POST['pacientes']))
{
	$q=$conexion->real_escape_string($_POST['pacientes']);
	$query="SELECT * FROM pacientes WHERE 
		num_paciente LIKE '%".$q."%' OR
		nombre_paciente LIKE '%".$q."%' OR
		apellido_paciente LIKE '%".$q."%' OR
		ci_paciente LIKE '%".$q."%' OR
		edad_paciente LIKE '%".$q."%' OR
        sexo_paciente LIKE '%".$q."%' OR
        telefono_paciente LIKE '%".$q."%' OR
		correo_paciente LIKE '%".$q."%' OR
        direccion_paciente LIKE '%".$q."%'" ;
}

$buscarPacientes=$conexion->query($query);
if ($buscarPacientes->num_rows > 0)
{
	$tabla.= 
	'<table class="table">
		<tr class="bg-primary">
			<td class="had">Nº</td>
			<td class="had">Nombres</td>
			<td class="had">Apellidos</td>
			<td class="had">CI</td>
			<td class="had">Edad</td>
            <td class="had">Sexo</td>
            <td class="had">Telefono</td>
			<td class="had">Correo</td>
            <td class="had">Direccion</td>
            <td colspan="2" class="had">Operaciones</td>
		</tr>';

	while($filaPacientes= $buscarPacientes->fetch_assoc())
	{
		$tabla.=
		'<tr class="bg-danger">
			<td class="">'.$filaPacientes['num_paciente'].'</td>
			<td>'.$filaPacientes['nombre_paciente'].'</td>
			<td>'.$filaPacientes['apellido_paciente'].'</td>
			<td>'.$filaPacientes['ci_paciente'].'</td>
			<td>'.$filaPacientes['edad_paciente'].'</td>
            <td>'.$filaPacientes['sexo_paciente'].'</td>
            <td>'.$filaPacientes['telefono_paciente'].'</td>
			<td>'.$filaPacientes['correo_paciente'].'</td>
    		<td>'.$filaPacientes['direccion_paciente'].'</td>
            <td><a href="modificar_pacientes.php?num_paciente='.$filaPacientes['num_paciente'].'" class="icon-address-book"></a></td>
            <td><a href="eliminar_pacientes.php?num_paciente='  .$filaPacientes['num_paciente'].'" class="icon-trash-1"></a></td>
		 </tr>
		';
	}

	$tabla.='</table>';
} else
	{
		$tabla="<h2>No se encontraron coincidencias con sus criterios de búsqueda.</h2>";
	}


echo $tabla;

                        
                    ?>

            
            </table>
            </div>
        
        
    
  
    
    
    </body>

</html>
