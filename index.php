<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Iniciar Sesion</title>
    <link rel="stylesheet" type="text/css" href="css/st_login.css">
    <link rel="stylesheet" href="css/fontello.css">
    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="js/valid_login.js"></script>

</head>
<body>



    <div class="content">
        <h2>Iniciar Sesion</h2>
        <form action="process_login.php" method="POST">
            <label for="user">Usuario</label>
            <input type="text" name="usuario" id="usuario" >
            <div id="msj1"></div>
            <label for="password">Contraseña</label>
            <input type="password" name="contrasena" id="contrasena">
            <div  id="msj2">
            </div>
            <input type="submit" value="Entrar" id="enviar">
        </form>

    </div>
    
</body>
</html>