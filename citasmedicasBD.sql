-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 06-12-2018 a las 15:08:08
-- Versión del servidor: 10.1.37-MariaDB
-- Versión de PHP: 7.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `citasmedicas`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `citas`
--

CREATE TABLE `citas` (
  `num` int(11) NOT NULL,
  `nom_paciente` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `nom_doctor` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `id_consul` int(50) NOT NULL,
  `time` time(4) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `citas`
--

INSERT INTO `citas` (`num`, `nom_paciente`, `nom_doctor`, `id_consul`, `time`, `date`) VALUES
(5, 'Edward Cabrera', 'Chapatin', 333, '12:30:00.0000', '2019-03-31'),
(7, 'Victoria Lenny', 'Juanito', 56213, '04:05:00.0000', '2013-12-13'),
(9, 'Pulgarcito Cabrera', 'Mario', 45812, '05:45:00.0000', '0051-05-04'),
(11, 'Lara', 'Juancito', 1212, '13:03:00.0000', '3223-02-12'),
(12, 'luisito papi Rey', 'Daniel', 456785, '01:58:00.0000', '2018-12-01'),
(13, 'Luis', 'Ale', 0, '00:00:00.0000', '0000-00-00'),
(14, 'dsad', '', 0, '00:00:00.0000', '0000-00-00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `consultorios`
--

CREATE TABLE `consultorios` (
  `num_consultorio` int(11) NOT NULL,
  `id_consultorio` int(50) NOT NULL,
  `nombre_consultorio` varchar(50) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `consultorios`
--

INSERT INTO `consultorios` (`num_consultorio`, `id_consultorio`, `nombre_consultorio`) VALUES
(4, 451245, 'OftalmolÃ³gico'),
(5, 7744, 'Victoria'),
(6, 7744, 'Victoria');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `doctores`
--

CREATE TABLE `doctores` (
  `num_doctor` int(11) NOT NULL,
  `nombre_doctor` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `apellido_doctor` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `ci_doctor` int(50) NOT NULL,
  `edad_doctor` int(50) NOT NULL,
  `sexo_doctor` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `telefono_doctor` int(50) NOT NULL,
  `correo_doctor` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `direccion_doctor` varchar(50) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `doctores`
--

INSERT INTO `doctores` (`num_doctor`, `nombre_doctor`, `apellido_doctor`, `ci_doctor`, `edad_doctor`, `sexo_doctor`, `telefono_doctor`, `correo_doctor`, `direccion_doctor`) VALUES
(4, 'Vergil', 'Rios', 4548783, 1, 'Masculino', 56320, 'v@777.com', 'Blanco Galindo3.5km');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pacientes`
--

CREATE TABLE `pacientes` (
  `num_paciente` int(11) NOT NULL,
  `nombre_paciente` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `apellido_paciente` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `ci_paciente` int(50) NOT NULL,
  `edad_paciente` int(50) NOT NULL,
  `sexo_paciente` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `telefono_paciente` int(50) NOT NULL,
  `correo_paciente` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `direccion_paciente` varchar(50) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `pacientes`
--

INSERT INTO `pacientes` (`num_paciente`, `nombre_paciente`, `apellido_paciente`, `ci_paciente`, `edad_paciente`, `sexo_paciente`, `telefono_paciente`, `correo_paciente`, `direccion_paciente`) VALUES
(2, 'lui', 'cabrera', 455, 25, '#', 1245, 'lui@45.com', 'hojo'),
(5, 'Ana Juana', 'Rios Renjifo', 14035872, 19, 'femenino', 76126, 'ana@19.com', 'cercado-CBBA'),
(6, 'Ana Juana', 'Rios Renjifo', 14035872, 20, 'Femenino', 76126, 'ana@19.com', 'cercado-CBBA'),
(7, 'Jose Luis', 'Rios Renjifo', 4512154, 49, 'Masculino', 4512485, 'j@rios.com', 'Puente de quillacollo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sesion`
--

CREATE TABLE `sesion` (
  `id` int(50) NOT NULL,
  `user` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `password` varchar(30) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `sesion`
--

INSERT INTO `sesion` (`id`, `user`, `password`) VALUES
(1, 'admin', '6667');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `citas`
--
ALTER TABLE `citas`
  ADD PRIMARY KEY (`num`);

--
-- Indices de la tabla `consultorios`
--
ALTER TABLE `consultorios`
  ADD PRIMARY KEY (`num_consultorio`);

--
-- Indices de la tabla `doctores`
--
ALTER TABLE `doctores`
  ADD PRIMARY KEY (`num_doctor`);

--
-- Indices de la tabla `pacientes`
--
ALTER TABLE `pacientes`
  ADD PRIMARY KEY (`num_paciente`);

--
-- Indices de la tabla `sesion`
--
ALTER TABLE `sesion`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `citas`
--
ALTER TABLE `citas`
  MODIFY `num` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT de la tabla `consultorios`
--
ALTER TABLE `consultorios`
  MODIFY `num_consultorio` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `doctores`
--
ALTER TABLE `doctores`
  MODIFY `num_doctor` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `pacientes`
--
ALTER TABLE `pacientes`
  MODIFY `num_paciente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `sesion`
--
ALTER TABLE `sesion`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
