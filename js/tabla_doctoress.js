$(obtener_registros());

function obtener_registros(doctores)
{
	$.ajax({
		url : 'tabla_doctoress.php',
		type : 'POST',
		dataType : 'html',
		data : { doctores: doctores },
		})

	.done(function(resultado){
		$("#tabla_resultadoo").html(resultado);
	})
}

$(document).on('keyup', '#busquedas', function()
{
	var valorBusqueda=$(this).val();
	if (valorBusqueda!="")
	{
		obtener_registros(valorBusqueda);
	}
	else
		{
			obtener_registros();
		}
});
