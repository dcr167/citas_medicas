$(obtener_registros());

function obtener_registros(doctores)
{
	$.ajax({
		url : 'tabla_doctores.php',
		type : 'POST',
		dataType : 'html',
		data : { doctores: doctores },
		})

	.done(function(resultado){
		$("#tabla_resultado").html(resultado);
	})
}

$(document).on('keyup', '#busqueda', function()
{
	var valorBusqueda=$(this).val();
	if (valorBusqueda!="")
	{
		obtener_registros(valorBusqueda);
	}
	else
		{
			obtener_registros();
		}
});
