$(obtener_registros());

function obtener_registros(consultorios)
{
	$.ajax({
		url : 'tabla_consultorios.php',
		type : 'POST',
		dataType : 'html',
		data : { consultorios: consultorios },
		})

	.done(function(resultado){
		$("#tabla_resultado").html(resultado);
	})
}

$(document).on('keyup', '#busqueda', function()
{
	var valorBusqueda=$(this).val();
	if (valorBusqueda!="")
	{
		obtener_registros(valorBusqueda);
	}
	else
		{
			obtener_registros();
		}
});
